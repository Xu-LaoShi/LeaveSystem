//package com.YangYu.sys.controller;
//
//import com.YangYu.sys.entity.User;
//import com.YangYu.sys.service.UserService;
//import com.YangYu.sys.service.exception.BussinessException;
//import com.YangYu.sys.utils.FileUtil;
//import com.YangYu.sys.utils.ImportExcelUtil;
//import com.alibaba.fastjson.JSON;
//import org.apache.commons.fileupload.FileItem;
//import org.apache.commons.fileupload.FileItemFactory;
//import org.apache.commons.fileupload.disk.DiskFileItemFactory;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//
//import java.io.InputStream;
//import java.util.HashMap;
//import java.util.List;
//import jxl.Sheet;
//import jxl.Workbook;
//
///**
// * @Description: 注册接口
// * Author: XX
// * Date: 2022/3/22  16:17
// **/
//public class RegisterServlet extends HttpServlet {
//    Logger logger = LoggerFactory.getLogger(LoginServlet.class);
//    private UserService userService = new UserService();
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.setCharacterEncoding("utf-8");
//        response.setContentType("text/html;charset=utf-8");
//        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
//        if(isMultipart){
//            String realpath = request.getSession().getServletContext().getRealPath("/files");
//            System.out.println(realpath);
//            File dir = new File(realpath);
//            if(!dir.exists()) {
//                dir.mkdirs();
//            }
//
//            FileItemFactory factory = new DiskFileItemFactory();
//            ServletFileUpload upload = new ServletFileUpload(factory);
//            upload.setHeaderEncoding("UTF-8");
//            try {
//                List<FileItem> items = upload.parseRequest(request);
//                for(FileItem item : items){
//                    if(item.isFormField()){
//                        String name1 = item.getFieldName();//得到请求参数的名称
//                        String value = item.getString("UTF-8");//得到参数值
//                        System.out.println(name1+ "="+ value);
//                    }else{
//                        File file = new File(dir, System.currentTimeMillis() + item.getName().substring(item.getName().lastIndexOf(".")));
//                        item.write(new File(dir, String.valueOf(file)));
//                        //得到excel文件 file
//                        Workbook rwb = Workbook.getWorkbook(file);
//                        Sheet rs = rwb.getSheet(0);//或者rwb.getSheet(0)
//                        int clos = rs.getColumns();//得到所有的列
//                        if (clos != 3) {
//                            return ApiRestResponse.error(AITExceptionEnum.EXCEL_FORMAT_ERROR);
//                        }
//                        InputStream fileis = new FileInputStream(file);
////                        InputStream in = new InputStream(new FileInputStream(file));
//                        List<List<String>> listByExcel = ImportExcelUtil.getBankListByExcel(fileis,file.getName());
//                        int count = 0;
//                        long start = System.currentTimeMillis();
//                        System.out.println(start);
//                        userService.userBatch(listByExcel);
//                    /*    while (listByExcel.iterator().hasNext() && count < listByExcel.size()) {
//                            List<String> list = listByExcel.get(count);
//                            String userName = list.get(0);
//                            String userPassword = list.get(1);
//                            String Name = list.get(2);
//                            String department = list.get(3);
//                            userService.userBatch(userName, userPassword, Name, Integer.parseInt(department));
//                            System.out.println("userName:" + userName + "   userPassword:" + userPassword + "    name:" + Name);
//                            count++;
//                        }*/
//                        long end = System.currentTimeMillis();
//                        System.out.println("时长：" + (start - end));
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }else{
//            doGet(request, response);
//        }
//            }
//
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        doPost(request,response);
//    }
//}
