package com.YangYu.sys.controller;

import com.YangYu.sys.service.ValidService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: TODO
 * Author: XX
 * Date: 2022/3/23  18:33
 **/
@WebServlet(name = "ValidServlet",urlPatterns = "/valid/*")
public class ValidServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        // http://localhost/leave/create
        String uri = request.getRequestURI();
        String methodName = uri.substring(uri.lastIndexOf("/") + 1);
        if (methodName.equals("isValid")) {
            this.isValid(request, response);
        }
    }

    private void isValid(HttpServletRequest request, HttpServletResponse response) {
        String formId = request.getParameter("formId");
        String validCode = request.getParameter("validCode");
        ValidService validService = new ValidService();
        validService.isValid(Integer.parseInt(formId),validCode);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
