//package com.YangYu.sys.utils;
//
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.imageio.ImageIO;
//import javax.imageio.stream.ImageInputStream;
//import java.io.*;
//import java.util.Iterator;
//
//public class FileUtil {
//
//    /**
//     * 将前端传入文件 转换为 InputStream
//     * @param file MultipartFile：前端传入文件类型
//     * @return
//     * @throws IOException
//     */
////    public static InputStream MultipartFileTOInputStream(MultipartFile file) throws IOException {
////
////        InputStream ips = null;
////        File file1 = null;
////        file1 = File.createTempFile("temp", null);
////        //transferTo,把文件上传到对应的目录下。
////        file.transferTo(file1);
////        ips =new FileInputStream(file1);
////        file1.deleteOnExit();
////        return ips;
////
////    }
//
//
//    /**
//     * 删除文件夹或目录
//     * @param folderPath 文件夹完整绝对路径
//     */
//    public static void delFolder(String folderPath) {
//        try {
//            delAllFile(folderPath); // 删除完里面所有内容
//            String filePath = folderPath;
//            filePath = filePath.toString();
//            File myFilePath = new File(filePath);
//            myFilePath.delete(); // 删除空文件夹
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    /**
//     * 删除指定文件夹下所有文件
//     * @param path 文件夹完整绝对路径
//     * @return
//     */
//    public static boolean delAllFile(String path) {
//        boolean flag = false;
//        File file = new File(path);
//        if (!file.exists()) {
//            return flag;
//        }
//        if (!file.isDirectory()) {
//            return flag;
//        }
//        String[] tempList = file.list();
//        File temp = null;
//        for (int i = 0; i < tempList.length; i++) {
//            if (path.endsWith(File.separator)) {
//                temp = new File(path + tempList[i]);
//            } else {
//                temp = new File(path + File.separator + tempList[i]);
//            }
//            if (temp.isFile()) {
//                temp.delete();
//            }
//            if (temp.isDirectory()) {
//                delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
//                delFolder(path + "/" + tempList[i]);// 再删除空文件夹
//                flag = true;
//            }
//        }
//        return flag;
//    }
//
//    /**
//     * MultipartFile 转 File
//     *
//     * @param file
//     * @throws Exception
//     */
//    public static File multipartFileToFile(MultipartFile file) throws Exception {
//
//        File toFile = null;
//        if (file.equals("") || file.getSize() <= 0) {
//            file = null;
//        } else {
//            InputStream ins = null;
//            ins = file.getInputStream();
//            toFile = new File(file.getOriginalFilename());
//            inputStreamToFile(ins, toFile);
//            ins.close();
//        }
//        return toFile;
//    }
//    //获取流文件
//    private static void inputStreamToFile(InputStream ins, File file) {
//        try {
//            OutputStream os = new FileOutputStream(file);
//            int bytesRead = 0;
//            byte[] buffer = new byte[8192];
//            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
//                os.write(buffer, 0, bytesRead);
//            }
//            os.close();
//            ins.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    /**
//     * @param file 文件路径
//     * @return 是否是图片
//     */
//    public static boolean isImage(File file) throws IOException {
//        if (file!=null && file.exists() && file.isFile()) {
//            ImageInputStream iis = null;
//            try {
//                iis = ImageIO.createImageInputStream(file);
//                Iterator iter = ImageIO.getImageReaders(iis);
//                if (iter.hasNext()) {
//                    return true;
//                }
//            } catch (IOException e) {
//                return false;
//            }finally{
//                iis.close();
//            }
//
//        }
//        return false;
//    }
//
//}
//
//
