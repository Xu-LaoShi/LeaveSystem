package com.YangYu.sys.entity;

/**
 * @Description: TODO
 * Author: XX
 * Date: 2022/3/23  16:34
 **/
public class Valid {
    int id;
    int leaveFormId;
    String validCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLeaveFormId() {
        return leaveFormId;
    }

    public void setLeaveFormId(int leaveFormId) {
        this.leaveFormId = leaveFormId;
    }

    public String getValidCode() {
        return validCode;
    }

    public void setValidCode(String validCode) {
        this.validCode = validCode;
    }
}
