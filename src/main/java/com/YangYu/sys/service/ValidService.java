package com.YangYu.sys.service;

import com.YangYu.sys.dao.ValidDao;
import com.YangYu.sys.entity.Valid;
import com.YangYu.sys.utils.MD5Utils;
import com.YangYu.sys.utils.MybatisUtils;

/**
 * @Description: TODO
 * Author: XX
 * Date: 2022/3/23  16:33
 **/
public class ValidService {

   public int insertVaidCode(int formId) {
       String validCode = MD5Utils.md5Digest(String.valueOf(System.currentTimeMillis()));
        return (int)MybatisUtils.executeUpdate(sqlSession -> {
           ValidDao validDao = sqlSession.getMapper(ValidDao.class);
            Valid valid = new Valid();
            valid.setLeaveFormId(formId);
            valid.setValidCode(validCode);
            return validDao.insertValidCode(valid);
       });
    }

    public boolean isValid(int formId, String validCode) {
        return(boolean)MybatisUtils.executeQuery(sqlSession -> {
           ValidDao validDao = sqlSession.getMapper(ValidDao.class);
           Valid valid = validDao.isValid(formId, validCode);
           if (valid != null){
               return true;
           }
           return false;
       });
    }
}
