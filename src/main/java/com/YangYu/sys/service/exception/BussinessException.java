package com.YangYu.sys.service.exception;

/**
 * 业务逻辑异常
 */
public class BussinessException extends RuntimeException{
    private String code; //异常编码,异常的以为标识
    private String message; //异常的具体文本消息

    /**
     *  l001 用户名不存在
     *  l002 密码错误
     *  l003 用户名已存在
     *  l004 密码长度不能小于6
     *
     */

    public BussinessException(String code,String msg){
        super(code + ":" + msg);
        this.code = code;
        this.message = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
