package com.YangYu.sys.service;

import com.YangYu.sys.dao.EmployeeDao;
import com.YangYu.sys.dao.LeaveFormDao;
import com.YangYu.sys.dao.RbacDao;
import com.YangYu.sys.dao.UserDao;
import com.YangYu.sys.entity.Employee;
import com.YangYu.sys.entity.Node;
import com.YangYu.sys.entity.User;
import com.YangYu.sys.service.exception.BussinessException;
import com.YangYu.sys.utils.MD5Utils;
import com.YangYu.sys.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * 用户服务
 */
public class UserService {

//    private UserDao userDao = SqlSession.getMapper(UserDao.class);
    private RbacDao rbacDao = new RbacDao();
    /**
     * 根据前台输入进行登录校验
     * @param username 前台输入的用户名
     * @param password 前台输入的密码
     * @return 校验通过后,包含对应用户数据的User实体类
     * @throws BussinessException L001-用户名不存在,L002-密码错误
     */
    public User checkLogin(String username , String password){
       return (User)MybatisUtils.executeQuery(sqlSession -> {
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            //按用户名查询用户
            User user = userDao.selectByUsername(username);
            if(user == null){
                //抛出用户不存在异常
                throw new BussinessException("L001", "用户名不存在");
            }
            //对前台输入的密码加盐混淆后生成MD5,与保存在数据库中的MD5密码进行比对
            String md5 = MD5Utils.md5Digest(password, user.getSalt());
            if(!md5.equals(user.getPassword())){
                //密码错误的情况
                throw new BussinessException("L002", "密码错误");
            }
            return user;
        });
//        //按用户名查询用户
//        User user = userDao.selectByUsername(username);
//        if(user == null){
//            //抛出用户不存在异常
//            throw new BussinessException("L001", "用户名不存在");
//        }
//        //对前台输入的密码加盐混淆后生成MD5,与保存在数据库中的MD5密码进行比对
//        String md5 = MD5Utils.md5Digest(password, user.getSalt());
//        if(!md5.equals(user.getPassword())){
//            //密码错误的情况
//            throw new BussinessException("L002", "密码错误");
////        }
//        return user;
    }

    public List<Node> selectNodeByUserId(Long userId){
        List<Node> nodeList = rbacDao.selectNodeByUserId(userId);
        return nodeList;
    }

    /**
     * 用户注册接口
     * @param username  用户名
     * @param password  用户密码
     * @return
     */
    public User register(String username, String password) {
        return (User)MybatisUtils.executeQuery(sqlSession -> {
                    UserDao userDao = sqlSession.getMapper(UserDao.class);
            //按用户名查询用户
            User user = userDao.selectByUsername(username);
            if(user != null){
                //抛出用户不存在异常
                throw new BussinessException("L003", "用户名已存在");
            }
            //对前台输入的密码加盐混淆后生成MD5,与保存在数据库中的MD5密码进行比对
            String md5 = MD5Utils.md5Digest(password, user.getSalt());
            if(password.length()<6){
                //密码错误的情况
                throw new BussinessException("L004", "密码长度不能小于6");
            }
            userDao.insertUser(username,password);
            return user;
                });

//        //按用户名查询用户
//        User user = userDao.selectByUsername(username);
//        if(user != null){
//            //抛出用户不存在异常
//            throw new BussinessException("L003", "用户名已存在");
//        }
//        //对前台输入的密码加盐混淆后生成MD5,与保存在数据库中的MD5密码进行比对
//        String md5 = MD5Utils.md5Digest(password, user.getSalt());
//        if(password.length()<6){
//            //密码错误的情况
//            throw new BussinessException("L004", "密码长度不能小于6");
//        }
//        userDao.insertUser(username,password);
//        return user;
    }

    public void userBatch(String userName, String userPassword, String name, int department) {
        MybatisUtils.executeUpdate(sqlSession -> {
            EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
            return null;
        });
        MybatisUtils.executeUpdate(sqlSession -> {
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            return null;
        });
        MybatisUtils.executeUpdate(sqlSession -> {
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            return null;
        });
    }

    public void userBatch(List<List<String>> listByExcel) {
        List<Employee> employeelist = new ArrayList<>();
        for (int i = 0; i < listByExcel.size(); i++) {
            List<String> userInf = listByExcel.get(i);
            Employee employee = new Employee();
            employee.setLevel(1);
            String userName = userInf.get(0);
            String password = userInf.get(1);
            String name = userInf.get(2);
            employee.setName(name);
            String department = userInf.get(3);
            if (department.equals(1)){
                employee.setTitle("软件学院学生");
            }else if (department.equals(2)){
                employee.setTitle("体育学院学生");
            }
            employee.setEmployeeId(Long.valueOf(department));
            employeelist.add(employee);
        }
        MybatisUtils.executeUpdate(sqlSession -> {
            EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
            int i = employeeDao.insertBatch(employeelist);
            return i;
        });

    }
}
