package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Notice;


import java.util.List;

public interface NoticeDao {

    public void insert(Notice notice);

    public List<Notice> selectByReceiverId(Long receiverId);
}
