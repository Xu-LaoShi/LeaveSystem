package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Node;
import com.YangYu.sys.utils.MybatisUtils;


import java.util.List;

public class RbacDao {
    public List<Node> selectNodeByUserId(Long userId){
        return (List) MybatisUtils.executeQuery(sqlSession -> sqlSession.selectList("rbacmapper.selectNodeByUserId", userId));
    }
}
