package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Department;


public interface DepartmentDao {
    public Department selectById(Long departmentId);
}
