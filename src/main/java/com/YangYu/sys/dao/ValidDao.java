package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Valid;
import org.apache.ibatis.annotations.Param;

public interface ValidDao {

    int insertValidCode(@Param("valid") Valid valid);

    Valid isValid(@Param("formId") int formId, @Param("validCode") String validCode);
}
