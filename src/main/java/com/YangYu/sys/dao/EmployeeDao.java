package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Employee;

import org.apache.ibatis.annotations.Param;
import org.apache.xmlbeans.impl.xb.xsdschema.ListDocument;

import java.beans.Transient;
import java.util.List;

public interface EmployeeDao {
    public Employee selectById(Long employeeId);

    /**
     * 根据传入员工对象获取上级主管对象
     * @param employee 员工对象
     * @return 上级主管对象
     */
    public Employee selectLeader(@Param("emp") Employee employee);
    @Transient
    int insertBatch(@Param("employee") List<Employee> employee);
}
