package com.YangYu.sys.dao;

import com.YangYu.sys.entity.Notice;
import com.YangYu.sys.utils.MybatisUtils;

import org.junit.Test;

import java.util.Date;

public class NoticeDaoTest {
    @Test
    public void testInsert(){
        MybatisUtils.executeUpdate(sqlSession -> {
            NoticeDao dao = sqlSession.getMapper(NoticeDao.class);
            Notice notice = new Notice();
            notice.setReceiverId(2l);
            notice.setContent("测试消息");
            notice.setCreateTime(new Date());
            dao.insert(notice);
            return null;
        });
    }

}
