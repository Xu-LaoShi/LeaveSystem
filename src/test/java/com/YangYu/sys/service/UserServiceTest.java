package com.YangYu.sys.service;

import com.YangYu.sys.entity.Node;
import com.YangYu.sys.entity.User;

import org.junit.Test;

import java.util.List;

public class UserServiceTest {
    private UserService userService = new UserService();
    @Test
    public void checkLogin1() {
        userService.checkLogin("uu", "123456");
    }

    @Test
    public void checkLogin2() {
        userService.checkLogin("m8", "123456");
    }

    @Test
    public void checkLogin3() {
        User user = userService.checkLogin("t5", "test");
        System.out.println(user);
    }

    @Test
    public void selectNodeByUserId(){
        List<Node> nodeList = userService.selectNodeByUserId(2l);
        System.out.println(nodeList);
    }
}
